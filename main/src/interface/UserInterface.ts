/*
 * @Author: lizhijie429
 * @Date: 2021-07-21 09:32:06
 * @LastEditors: lizhijie429
 * @LastEditTime: 2021-07-22 15:34:12
 * @Description:
 */

export interface InterUserInfo {
  name: string;
  age: number;
}
export interface InterGlobalConfig {
  size: string;
  zIndex: number;
}
