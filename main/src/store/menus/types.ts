/*
 * @Author: lizhijie429
 * @Date: 2021-07-22 20:25:53
 * @LastEditors: lizhijie429
 * @LastEditTime: 2021-07-23 10:24:13
 * @Description:
 */
export enum MenusMutationsType {
  SET_ROUTERS_LIST = "SET_ROUTERS_LIST",
  SET_MENUS_LIST = "SET_MENUS_LIST",
  UPDATE_SUB_MENU = "UPDATE_SUB_MENU",
  SET_CURRENT_APP = "SET_CURRENT_APP",
  SET_CURRENT_PAGE = "SET_CURRENT_PAGE",
}
